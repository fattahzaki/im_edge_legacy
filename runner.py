"""

Running the code:
$> python3 runner.py -c config_temasya.json

"""
import cv2

import logging
import boto3
from botocore.exceptions import ClientError

import numpy as np
from skimage.metrics import structural_similarity
from utils import nice_time
import os
import time
import datetime as datetime
import random  # to randomize the camera resets
from threading import Thread
import requests
import json
import uuid
import argparse

url = "http://localhost:3000/api/v2/receive"
headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
os.environ['CUDA_VISIBLE_DEVICES'] = '1'

# construct the argument parser and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-c", "--config", type=str, default="config.json",
                help="minimum confidence that the detection is a face")
args = vars(ap.parse_args())

with open(args['config'], 'r') as f:
    json_content = json.load(f)

dbg = 0
color = (0, 255, 255)
font = cv2.FONT_HERSHEY_PLAIN
MAX_TIME = 5

MAX_SETTLED = json_content['max_settled']
MAX_MOVEMENT = json_content['max_settled']
MAX_SC = json_content['max_sc_threshold']
SEND_TO_CLOUD = json_content['send_to_cloud']
locfile = json_content['locfile']
locfile1 = json_content['loc_file1']
pumps_data = json_content['pumps']

loop_time = time.time()
random.seed(time.time())

WH = 80
MAXLEN = 3
INTIME = 5  # secs

pump = []         
pumpid = {
    'P01': str(uuid.uuid4()),
    'P02': str(uuid.uuid4()),
    'P03': str(uuid.uuid4()),
    'P04': str(uuid.uuid4()),
    'P05': str(uuid.uuid4()),
    'P06': str(uuid.uuid4()),
    'P07': str(uuid.uuid4()),
    'P08': str(uuid.uuid4()),
    'P09': str(uuid.uuid4()),
    'P10': str(uuid.uuid4()),
    'P11': str(uuid.uuid4()),
    'P12': str(uuid.uuid4())
}

# Description of the filling pumps unique id
pumpstats = {'P01': False, 'P02': False, 'P03': False, 'P04': False,
             'P05': False, 'P06': False, 'P07': False, 'P08': False,
             'P09': False, 'P10': False, 'P11': False, 'P12': False}

# Description of the filling pumps status
p_iter = []       # individual iteration count for each pump for resetting camera
cap = []          # Handle for CCTV
video = []        # record video at filling pump
out = []          # video file per pump
in_fill = []      # Flag for whether filling pump occupied
in_time = []      # Time when vehicle entered pump area
out_time = []     # Time when vehicle leaves pump area
settled = []      # Number of iterations since last movement detecte around pump area
movement = []     # Have we detected any movement lately?
within = []       # Is there a vehicle within the pump area
current_img = []
prev_img = []
stationary_img = []  # Images used to check for movements
chk_h = []
chk_w = []  # Height and Width of the region being checked

# PARTICULARS FOR PUMP 2 700x800
pump.append("P02")
video.append(False)
out.append(0)

# PARTICULARS FOR PUMP 3
pump.append("P03")
video.append(False)
out.append(0)

# PARTICULARS FOR PUMP 4
pump.append("P04")
video.append(False)
out.append(0)

# PARTICULARS FOR PUMP 5 700x800
pump.append("P05")
video.append(False)
out.append(0)

# PARTICULARS FOR PUMP 6 700x800
pump.append("P06")
video.append(False)
out.append(0)

# PARTICULARS FOR PUMP 7 700x800
pump.append("P07")
video.append(False)
out.append(0)

# PARTICULARS FOR PUMP 8 700x800
pump.append("P08")
video.append(False)
out.append(0)

# PARTICULARS FOR PUMP 9 800x800
pump.append("P09")
video.append(False)
out.append(0)

# gray=frame[600:1400,300:1100] PUMP 10
pump.append("P10")
video.append(False)
out.append(0)

# gray=frame[200:760,1200:1760] P11
pump.append("P11")
video.append(False)
out.append(0)

# gray=frame[300:700,750:1150] P12
pump.append("P12")
video.append(False)
out.append(0)

# set these to false to minimise the windows on show.
# 2,3,4,5,6,7,8
video[3] = True  # Pump 5
video[4] = False  # Pump 6
video[8] = True

# endregion

# region FUNCTIONS


def point_in_rect(x, y, rect):
    h, w, ch = rect.shape
    x1 = int(w/4)
    y1 = int(h/4)
    x2, y2 = int(w/4)*3, int(h/4)*3

    if (x1 < x and x < x2):
        if (y1 < y and y < y2):
            return True
    return False

# Yolo stuff starts here ===========================
classes = []
with open("model_data/coco/coco.names", "r") as f:
    classes = [line.strip() for line in f.readlines()]

yolo = cv2.dnn.readNet("model_data/yolov3.weights", "model_data/yolov3.cfg")
# yolo = cv2.dnn.readNet("model_data/yolov4-aiguy-npr.weights",
#                       "model_data/yolov4-custom.cfg")

layer_names = yolo.getLayerNames()
output_layers = [layer_names[i[0] - 1] for i in yolo.getUnconnectedOutLayers()]


def send_API(data):
    try:
        sendata = {'sender': 'forecourtv2',
                   'id': data['id'], 'pump_number': data['pump_number'],
                   'license_plate': data['license_plate'], 'datetime': data['datetime'],
                   'car_type': data['car_type'], 'seconds_on_pump': 0, 'color': data['color'],
                   'sendtype': data['sendtype']}
        r = requests.post(url, data=json.dumps(sendata), headers=headers)

    except:
        pass


def upload_file(local_file, bucket, folders, s3_file=None, delete_file=False):
    """
    Upload a file to an S3 bucket

    :param file_name: File to upload
    :param bucket: Bucket to upload to e.g. 'pdb-im-rekognition'
    :param folders: folders within bucket e.g. 'vehicle/ROI/'
    :param object_name: S3 object name. If not specified then file_name is used
    :return: True if file was uploaded, else False
    """
    # If S3 object_name was not specified, use file_name
    if s3_file is None:
        s3_file = local_file

    # Upload the file
    s3_client = boto3.client('s3')
    try:
        response = s3_client.upload_file(local_file, bucket, folders)
    except ClientError as e:
        logging.error(f"Failed to upload file {local_file}. Raised {e}")
        return False

    if delete_file:
        try:
            logging.info(f"Deleting {local_file}")
            os.remove(local_file)
        except Exception as e:
            logging.error(f"Failed to remove file {local_file}. Raised {e}")
    return True

def detect_vehicle(imgV, iM, carsout):

    color = (0, 255, 255)
    height, width, c = imgV.shape
    if dbg > 5:
        print("[INFO] Detecting Vehicle at "+nice_time())
    start14 = time.time()
    # Detecting objects
    blob = cv2.dnn.blobFromImage(
        imgV, 0.00392, (416, 416), (0, 0, 0), True, crop=False)
    yolo.setInput(blob)
    outs = yolo.forward(output_layers)

    # Showing informations on the screen
    class_ids = []
    confidences = []
    boxes = []
    for out in outs:
        for detection in out:
            scores = detection[5:]
            class_id = np.argmax(scores)
            confidence = scores[class_id]
            if confidence > 0.5:
                # Object detected
                center_x = int(detection[0] * width)
                center_y = int(detection[1] * height)
                w = int(detection[2] * width)
                h = int(detection[3] * height)
                # Rectangle coordinates
                x = int(center_x - w / 2)
                y = int(center_y - h / 2)
                boxes.append([x, y, w, h])
                confidences.append(float(confidence))
                class_ids.append(class_id)

    indexes = cv2.dnn.NMSBoxes(boxes, confidences, 0.5, 0.4)
    colors = np.random.uniform(0, 255, size=(len(classes), 3))
    font = cv2.FONT_HERSHEY_PLAIN
    color = (0.255, 0)
    persons = 0
    max_area = 0
    for i in range(len(boxes)):
        if i in indexes:
            x, y, w, h = boxes[i]
            x = max(0, x)
            y = max(0, y)
            label = str(classes[class_ids[i]])
            if label in ['car', 'truck', 'bus']:
                print("[INFO] Detected object label: {}".format(label))

                persons += 1
                cx = int((x+w)/2)
                cy = int((y+h)/2)
                if dbg > 1:
                    cv2.rectangle(imgV, (x, y), (x + w, y + h), color, 2)
                    sss = label+" ("+str(cx)+","+str(cy)+") " + \
                        str(round(confidences[i], 2))
                    cv2.rectangle(imgV, (cx-10, cy-10),
                                  (cx+10, cy+10), (0, 255, 0), 2)
                    cv2.rectangle(imgV, (int(width/4), int(height/4)),
                                  (int(width/4)*3, int(height/4)*3), (255, 0, 0), 2)
                    cv2.putText(imgV, sss, (x, y + 30), font, 2, color, 3)

                if (point_in_rect(cx, cy, imgV)):
                    print("[INFO] Vehicle type {} detected at ({},{}) ".format(
                        label, cx, cy))
                    print("[INFO] Yolo took {:.4f}s".format(
                        time.time()-start14))
                    ctr = cx, cy
                    if (carsout == 1):
                        print("Writing out cars at {} at pump {} with (w,h) of ({},{}) (x,y) of ({},{})"
                              .format(nice_time(), pump[iM], w, h, x, y))
                        car = imgV[y:y+h, x:x+w]
                        cv2.imwrite(
                            locfile + "/cars/"+pump[iM]+"_"+label+"_"+nice_time()+".png", car)
                    return imgV, True, cx, cy, label

    return imgV, False, 0, 0, 'NA'


def img_area(image, x1, y1, x2, y2):
    return image[y1:y2, x1:x2]


def detect_vehicle_at_pump(frame, pump, i):

    #cv2.imshow("foo", frame)
    #cv2.imwrite("frame.jpg", frame)

    # obtain ROI based on camera's position at pump
    img = frame[pump['roi']['y1']:pump['roi']['y2'],
                pump['roi']['x1']:pump['roi']['x2']]

    # obtain reduced ROI for image comparison
    h, w = img.shape[:2]
    chk_w[i] = int(w/2)
    chk_h[i] = int(h/2)
    current_img[i] = img_area(
        img, (chk_w[i]-WH), chk_h[i]-WH, chk_w[i]+WH, chk_h[i]+WH
    )

    sss = ""

    #print("Pump {} at i={} and in_fill={} and video={} and vidcount={}".format(pump[i],i,in_fill[i],video[i],vid_count[i]))

    """
    if in_fill[i]==True and video[i]==True:
        #print(">>>>write out car image for pump {}".format(pump[i]))
        ssss=locfile + "/{}_{}_{:04d}.png".format(pump[i],datetime.datetime.utcfromtimestamp(in_time[i]).strftime('%Y-%m-%d-%H-%M-%S'),vid_count[i])
        status=cv2.imwrite(ssss,img)
        if status==False:
            print(">>>>Write-out failed for "+ssss)
        vid_count[i] +=1
    """

    # Perform structural comparison on 2 subsequent frames
    sc = structural_similarity(prev_img[i], current_img[i], multichannel=True)

    if sc < MAX_SC:
        ssss = "[INFO] {}: Movement Detected with sc +{:.4f}+ at +{}+ and p_iter of {} and movement of {}".format(
            pump['pump_no'], sc, nice_time(), p_iter[i], movement[i])
        print(ssss)
        sss += ssss
        movement[i] += 1
        if movement[i] >= MAX_MOVEMENT:
            movement[i] = MAX_MOVEMENT
        settled[i] = 0
    else:
        movement[i] -= 1
        if movement[i] < 0:
            movement[i] = 0
        settled[i] += 1
        """
        print("{} SETTLED count: {}, SC: {}, Frame_count: {}".format(
            pump['pump_no'], settled[i], sc, iter
        ))
        """

    if settled[i] > MAX_SETTLED:

        # Vehicle detected in the bay?
        if ((in_fill[i] == False) and (movement[i] == 0)):
            ssss = "[INFO] {}: Settled, now checking for vehicle at +{}+ with in_fill +{}+ and movement +{}+ frames_processed +{}+".format(
                pump['pump_no'], nice_time(), in_fill[i], movement[i], iter
            )
            print(ssss)
            sss += ssss

            # Check vehicle's present at particular pump's bay
            img, within[i], cx, cy, objecttype = detect_vehicle(img, i, 0)

            # Vehicle's presence was detected
            if within[i]:
                ssss = "[INFO] {}: Vehicle detected at +{}+ and within is +{}+ at iter of +{}+".format(
                    pump['pump_no'], nice_time(), within[i], iter)
                print("")
                print(ssss)
                print("")
                sss += ssss

                # Indicate the state of vehicle in the bay
                in_fill[i] = True
                in_time[i] = time.time()
                stationary_img[i] = current_img[i]

                # Generate UUID for generating files
                # for attr, roi, scene, in/out
                pumpid[str(pump['pump_no'])] = str(uuid.uuid4())

                # Write captured ROIs to directory - for logging purpose
                #roi_in = f"{locfile}/{result_folder}/{pump[i]}-{nice_time()}-IN_.png"
                rin = "{}-IN".format(str(pumpid[str(pump['pump_no'])]))
                roi_in = "{}/{}/{}.png".format(
                    locfile, result_folder, rin
                )
                #cv2.imwrite(roi_in, img)

                rsc = "{}-SC".format(str(pumpid[str(pump['pump_no'])]))
                roi_sc = "{}/{}/{}.png".format(
                    locfile, result_folder, rsc
                )
                #cv2.imwrite(roi_sc, current_img[i])

                #cv2.imwrite(locfile + "/results/"+pump[i]+"_"+nice_time()+"_IN.png",img)
                #cv2.imwrite(locfile1 + str(pumpid[str(pump[i])]) + ".png", img)

                pumpstats[str(pump['pump_no'])] = True

                # START ----- update to AWS ------
                data = {
                    'pumpID': str(pump['pump_no']),
                    'stationID': 'RYB0437',
                    'enter_timestamp': nice_time(),
                    'exit_timestamp': ""
                }

                attr = "attr-{}".format(str(pumpid[str(pump['pump_no'])]))
                jsonFile = "{}/{}/{}.json".format(
                    locfile, result_folder, attr
                )

                with open(jsonFile, 'w') as jsonF:
                    json.dump(data, jsonF)

                bucket = 'pdb-im-rekognition'

                if SEND_TO_CLOUD == 1:
                    print('[INFO] Uploading vehicle attribute {}'.format(jsonFile))
                    #ret = upload_file(jsonFile, bucket, 'vehicle/attr/{}'.format(f"{attr}.json"),
                    #                  delete_file=True)
                    ret = upload_file(jsonFile, bucket, 'vehicle/attr/{}'.format(f"{attr}.json"))
                    if ret:
                        print('Attribute uploading successful!')
                else:
                    print('[INFO] writing vehicle attribute {}'.format(jsonFile))

                """
                print('Uploading vehicle attribute {}'.format(jsonFile))
                ret = upload_file(jsonFile, bucket,
                                  'vehicle/attr/{}'.format(f"{attr}.json"))
                if ret:
                    print('Attribute uploading successful!')
                """

                roi = "ROI-{}".format(str(pumpid[str(pump['pump_no'])]))
                roi_image = "{}/{}/{}.png".format(
                    locfile, result_folder, roi
                )

                scene = "scene-{}".format(str(pumpid[str(pump['pump_no'])]))
                scene_image = "{}/{}/{}.png".format(
                    locfile, result_folder, scene
                )

                # Save ROI and scene images
                wrstatus = cv2.imwrite(scene_image, frame)
                wrstatus = cv2.imwrite(roi_image, img)

                if SEND_TO_CLOUD == 1:
                    print('[INFO] Uploading scene image {}'.format(scene_image))
                    ret = upload_file(scene_image, bucket,
                                      'vehicle/scene/{}'.format(f"{scene}.png"), delete_file=True)
                    if ret:
                        print('Scene uploading successful!')
                else:
                    print('[INFO] writing scene image {}'.format(scene_image))

                if SEND_TO_CLOUD == 1:
                    print('[INFO] Uploading ROI vehicle {}'.format(roi_image))
                    ret = upload_file(roi_image, bucket,
                                      'vehicle/ROI/{}'.format(f"{roi}.png"))
                    ret = upload_file(roi_image, "pdb-im-faces",
                                      'process/{}'.format(f"{roi}.png"), delete_file=True)
                    if ret:
                        print('ROI uploading successful!')
                else:
                    print('[INFO] Writing ROI vehicle {}'.format(roi_image))

                # END ------------
                # TODO: to enable again
                #vid_count[i] = 0

            else:
                settled[i] = 0
                print("[INFO] {} No vehicle detected, Frame_count: {}".format(
                    pump['pump_no'], iter))

        scx = structural_similarity(
            stationary_img[i], current_img[i], multichannel=True)
        sss += " SCX stat vs current +{:.4f}+".format(scx)
        # print(sss)

        # if ((scx < 0.65) and (movement[i] == 0) and (in_fill[i] == True)):
        if ((scx < 0.55) and (movement[i] == 0) and (in_fill[i] == True)):
            print("")
            ssss = ": *** Vehicle Exit detected at +{}+ with scx of {:.4f} and settled of {} at iter of {}".format(
                nice_time(), scx, settled[i], iter)
            print("[INFO]" + pump['pump_no'] + ssss)
            print("[INFO] Uploading Vehicle exit time ...")
            print("")

            # Update json file with the new exit time stamp
            attr = "attr-{}".format(str(pumpid[str(pump['pump_no'])]))
            jsonFile = "{}/{}/{}.json".format(
                locfile, result_folder, attr
            )

            with open(jsonFile, 'r') as jsonF:
                jsonData = json.load(jsonF)

            jsonData["exit_timestamp"] = nice_time()

            with open(jsonFile, 'w') as jsonF:
                json.dump(jsonData, jsonF)

            
            if SEND_TO_CLOUD == 1:
                bucket = 'pdb-im-rekognition'
                print('[INFO] Uploading vehicle attribute {} with exit timestamp'.format(jsonFile))
                ret = upload_file(jsonFile, bucket, 'vehicle/attr/{}'.format(f"{attr}.json"), delete_file=True)
                if ret:
                    print('Attribute uploading successful!')

            sss += ssss
            out_time[i] = time.time()
            in_fill[i] = False
            settled[i] = 0

            #roi_out = f"{locfile}/{result_folder}/{pump[i]}-{nice_time()}-OUT.png"
            rout = "{}-OUT".format(str(pumpid[str(pump['pump_no'])]))
            roi_out = "{}/{}/{}.png".format(
                locfile, result_folder, rout
            )
            #cv2.imwrite(roi_out, img)

            data = {
                'id': str(pumpid[str(pump['pump_no'])]), 'pump_number': pump['pump_no'],
                'license_plate': '', 'datetime': datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
                'car_type': '', 'seconds_on_pump': 0, 'color': '',
                'sendtype': 1}
            #Thread(target = plate_colour_db, args=(data,)).start()
            pumpstats[str(pump['pump_no'])] = False
            pumpid[str(pump['pump_no'])] = uuid.uuid4()

    """
    if (settled[i] % 50 == 0) and (settled[i] != 0):  # To cater for any glitches
        img, within[i], cx, cy, objecttype = detect_vehicle(img, i, 0)
        if (within[i]):
            stationary_img[i] = current_img[i]
            sss += ("Stationary image reset at {} for pump {} at iter of {}".format(
                nice_time(), pump[i], iter))
            print(
                pump[i]+":Stationary image reset at {} for pump {}".format(nice_time(), pump[i]))
    """

    prev_img[i] = current_img[i]
    return img


def cleanup_text(text):
    # strip out non-ASCII text so we can draw the text on the image
    # using OpenCV
    return "".join([c if ord(c) < 128 else "" for c in text]).strip()

# endregion for functions

# Define the starting variables for each pump
for i, p in enumerate(pumps_data):

    p_iter.append(random.randint(0, 50))
    in_fill.append(False)
    in_time.append(time.time())
    out_time.append(time.time())
    settled.append(0)
    movement.append(0)
    within.append(False)

    if p['detect'] == 1:
        print("pump {}".format(p["pump_no"]))
        print("camera {}".format(p["camera_source"]))
        print("ROI {}".format(p["roi"]))
        cap.append(cv2.VideoCapture(p["camera_source"]))
    else:
        cap.append(None)

    iter = 0

    if p['detect'] == 1:
        while (True):
            iter += 1
            ret, frame = cap[i].read()
            if ret == True or iter > 100:
                break

        area = frame[p['roi']['y1']:p['roi']['y2'],
                     p['roi']['x1']:p['roi']['x2']]

        if p['display'] == 1:
            cv2.imshow(p['pump_no'], area)
            cv2.moveWindow(p['pump_no'], 20, 20)

        chk_h0, chk_w0, ch0 = area.shape
        chk_h.append(int(chk_h0/2))
        chk_w.append(int(chk_w0/2))

        tmp1 = img_area(area, (chk_w[i]-WH),
                        chk_h[i]-WH, chk_w[i]+WH, chk_h[i]+WH)
        # prev_img.append(area.copy())
        # current_img.append(area.copy())
        prev_img.append(tmp1.copy())
        current_img.append(tmp1.copy())
        stationary_img.append(tmp1.copy())
        cv2.rectangle(stationary_img[i], (10, 10), (50, 50), (0, 255, 0), 10)

    else:
        chk_h.append(0)
        chk_w.append(0)

        prev_img.append(None)
        current_img.append(None)
        stationary_img.append(None)

# Add for the video out
"""
vid_count = []
for p in enumerate(pump):
    vid_count.append(0)
"""
# endregion for variable initialisation

# ===========================MAIN STARTS HERE=====================================
now = datetime.datetime.now()
result_folder = "results/"+now.strftime("%Y%m%d%H")

def main():
    iter = 0
    print(f"locfile = {locfile}")
    os.makedirs(locfile + "/" + result_folder, exist_ok=True)

    bailout = False
    start = time.time()
    while (True):
        iter_start = time.time()
        for i, p in enumerate(pumps_data):
            if p['detect'] == 1:
                p_iter[i] += 1

                # Fast-forward reading 5 frames per processing. Minimize processing.
                """
                for j in range(5):
                    iter += 1
                    #print("frame count: {}".format(iter))
                    ret, frame = cap[i].read()
                    if ret == False:
                        print("cctv ret==False at - "+nice_time()+" for "+p)
                        bailout = True:w
                """

                ret, frame = cap[i].read()
                iter += 1
                if ret == False:
                    print("cctv ret==False at - " + nice_time()+" for " + p["pump_no"])
                    bailout = True

                if ret == True:
                    frm = detect_vehicle_at_pump(frame, p, i)
                    pumnumber = p['pump_no']
                    if p['display'] == 1:
                        frm = cv2.resize(frm, None, fx=0.5, fy=0.5)
                        cv2.imshow(p['pump_no'], frm)

        if bailout:
            break

        k = cv2.waitKey(1)
        if k == ord("q"):
            break

    elapsed = time.time() - start
    print("[INFO] Vehicle detection completed in {:.2f} secs".format(elapsed))

    cv2.destroyAllWindows()
    for i, p in enumerate(pumps_data):
        if p['detect'] == 1:
            cap[i].release()


if __name__ == "__main__":
    main()
