import time
import datetime as datetime

def nice_time():
    now=datetime.datetime.now()
    return now.strftime("%Y%m%d_%H%M_%S")